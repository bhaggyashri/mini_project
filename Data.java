package com.greatLearning.assignment;

public class Data {
public String menu;
public double price;
public int code; 
public int quantity;

public Data(String menu, double price, int code, int quantity) {
	super();
	this.menu = menu;
	this.price = price;
	this.code = code;
	this.quantity = quantity;
}

public String getMenu() {
	return menu;
}

public void setMenu(String menu) {
	this.menu = menu;
}

public double getPrice() {
	return price;
}

public void setPrice(double price) {
	this.price = price;
}

public int getCode() {
	return code;
}

public void setCode(int code) {
	this.code = code;
}

public int getQuantity() {
	return quantity;
}

public void setQuantity(int quantity) {
	this.quantity = quantity;
}

}
